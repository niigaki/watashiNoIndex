<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Watashi no Proyects</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/portfolio-item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php
        //nav
        include("assets/nav.php");
    ?>

    <!-- Page Content -->
    <div class="container">


        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">プロジェクト</h1>
            </div>


           <div role="tabpanel">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist" id="superTabPanel">
                <li role="presentation" class="active"><a href="#local" aria-controls="local" role="tab" data-toggle="tab">Local</a></li>
                <li role="presentation"><a href="#remote" aria-controls="remote" role="tab" data-toggle="tab">Remote</a></li>
                <li role="presentation"><a href="#test" aria-controls="remote" role="tab" data-toggle="tab">Tests</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="local">

                   <?php
                        //nav
                        include("assets/local.php");
                    ?>

                </div>
                <div role="tabpanel" class="tab-pane" id="remote">

                    <?php
                        //nav
                        include("assets/remote.php");
                    ?>

                </div>
                <div role="tabpanel" class="tab-pane" id="test">

                    <?php
                        //nav
                        include("assets/test.php");
                    ?>

                </div>
              </div>

            </div>

        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Misael Eduardo Rojas Lopez</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!--   watashi no js -->
    <script>

         $(function () {
            $('#superTabPanel a:first').tab('show')
         })

        $('#superTabPanel a[href="#local"]').tab('show') // Select tab by name
        $('#superTabPanel a[href="#remote"]').tab('show') // Select tab by name
        $('#superTabPanel a[href="#test"]').tab('test') // Select tab by name
    </script>

</body>

</html>
