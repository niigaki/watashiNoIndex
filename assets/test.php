<br />

<div class="col-sm-3 col-xs-6">
    <a href="/joomla_test/test/administrator">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/mn_joomla_be.jpg" alt="Mexico-Now Joomla version" title="Mexico-Now Joomla version">
    </a>
</div>

<div class="col-sm-3 col-xs-6">
    <a href="/joomla_test/test">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/mn_joomla.jpg" alt="Mexico-Now Joomla version" title="Mexico-Now Joomla version">
    </a>
</div>

<div class="col-sm-3 col-xs-6">
    <a href="/joomla_test/experimental/administrator">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/joomla_development_be.jpg" alt="Joomla development" title="Joomla development">
    </a>
</div>

<div class="col-sm-3 col-xs-6">
    <a href="/joomla_test/experimental">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/joomla_development.jpg" alt="Joomla development" title="Joomla development">
    </a>
</div>
