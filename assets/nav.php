    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">プロジェクト</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                   <li>
                        <a href="/myadmin">MyAdmin</a>
                    </li>
                    <li>
                        <a href="/ampps/">Ampps</a>
                    </li>
                    <li>
                        <a href="/ampps-admin/">Admin</a>
                    </li>
                    <li>
                        <a href="https://www.mexico-now.com:2083">Mexiconow</a>
                    </li>

                    <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Conferences <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="http://mexicoindustriallocation.com:2083">ILS</a>
                            <a href="http://www.mexicoaerospacesummit.com/:2083">AES</a>
                            <a href="http://www.mexicoaerospaceconference.com:2083">AEC</a>
                            <a href="http://www.mexicoautosummit.com:2083">AIS</a>
                            <a href="http://www.mexicoautoconference.com:2083">AEC</a>
                            <a href="http://mexicosupplychainsummit.com:2083">SCS</a>
                        </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
