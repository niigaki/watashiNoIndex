<br />

<div class="col-sm-3 col-xs-6">
    <a href="http://www.mexico-now.com/joomla/administrator">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/mn_joomla_be.jpg" alt="Mexico-Now Joomla version" title="Mexico-Now Joomla version">
    </a>
</div>

<div class="col-sm-3 col-xs-6">
    <a href="http://www.mexico-now.com/joomla">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/mn_joomla.jpg" alt="Mexico-Now Joomla version"  title="Mexico-Now Joomla version">
    </a>
</div>

<div class="col-sm-3 col-xs-6">
    <a href="https://www.mexico-now.com/online/admin.php">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/jaws.jpg" alt="Mexico-Now Joomla version" title="Mexico-Now Joomla version">
    </a>
</div>


<div class="col-sm-3 col-xs-6">
    <a href="http://www.mexicoaerospacesummit.com/">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/conferences/2015/AES.jpg" alt="Mexico's Aerospace Summit"  title="Mexico's Aerospace Summit 2015">
    </a>
</div>

<div class="col-sm-3 col-xs-6">
    <a href="http://www.mexicoautoconference.com/">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/conferences/2015/AIS.jpg" alt="Mexico's Auto Industry Summit 2015"  title="Mexico's Auto Industry Summit 2015">
    </a>
</div>

<div class="col-sm-3 col-xs-6">
    <a href="http://www.mexico-now.com/">
        <img class="img-responsive portfolio-item img-thumbnail" src="assets/img/mn.png" alt="Mexico-now"  title="Mexico-now">
    </a>
</div>
